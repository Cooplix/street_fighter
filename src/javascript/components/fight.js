import {controls} from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  let hitPower = getRandomChance(1, 2);
  return fighter.attack * hitPower;
}

export function getBlockPower(fighter) {
  let blockChance = getRandomChance(1, 2);
  return fighter.defense * blockChance
}


function getRandomChance(min, max) {
  return Math.random() * (max- min) + min;
}