import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter) {
    const img = createFighterImage(fighter);
    const name = createElement({ tagName: 'h4' });
    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-details'
    });
    const fighterDetailsWrapper = createElement({
      tagName: 'div',
      className: 'fighter-details-wrapper'
    });
    // icons https://iconify.design/icon-sets/mdi/
    fighterDetails.innerHTML = `
    <div class="fighter-detail-cell">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false"  style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M22 2l-4.61 1.75l-6.93 6.93L14 14.22l6.92-6.93C22.43 5.78 22 2 22 2M8.33 10l-1.41 1.39l1.41 1.41l-5.65 5.66L6.21 22l5.66-5.66l1.41 1.42l1.42-1.42L8.33 10z" fill="#626262"/></svg>
    ${fighter.attack}</p>
  </div>
  <div class="fighter-detail-cell">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false"  style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M12 1L3 5v6c0 5.55 3.84 10.74 9 12c5.16-1.26 9-6.45 9-12V5l-9-4z" fill="#626262"/></svg>
    ${fighter.defense}</p>
  </div>
  <div class="fighter-detail-cell">
  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false"  style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.27 2 8.5C2 5.41 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.08C13.09 3.81 14.76 3 16.5 3C19.58 3 22 5.41 22 8.5c0 3.77-3.4 6.86-8.55 11.53L12 21.35z" fill="#626262"/></svg>
    ${fighter.health}</p>
  </div>
    `;
    name.innerText = fighter.name;
    fighterDetailsWrapper.append(name, fighterDetails);
    fighterElement.append(img, fighterDetailsWrapper);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
